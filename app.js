

const express = require('express');

// Setup server
const app = express();
const server = require('http').createServer(app);
const contact = require('./contact');
const router = express.Router();
const bodyParser = require('body-parser');

// const port = 8080;
const port = process.env.PORT || 8080;

const expressValidator = require('express-validator');
app.use(expressValidator());

// Start server
server.listen( port , function () {
  console.log('Express server listening on %d, in %s mode ', port);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false,
  strict: false,
}));

app.post('/api/contact', function (req, res) {

  req.checkBody('email');

  // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
  res.setHeader('Access-Control-Allow-Origin', 'https://photo.jagodakondratiuk.com');
  contact.send(req,res);

})

exports = module.exports = app;
