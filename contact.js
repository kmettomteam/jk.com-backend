
const express = require("express");
const bodyParser = require('body-parser');
const path = require('path');

const nodemailer = require('nodemailer');

const smtpConfig = {
    host: 'smtp.websupport.sk',
    port: 465,
    secure: true, // use SSL
    auth: {
        user: 'hello@jagodakondratiuk.com',
        pass: 'Jaga5242'
    }
};

const transporter = nodemailer.createTransport(smtpConfig);

exports.send = function(req,res){

  var htmlContentMailHome = '<p>' + req.body.message + '</p>';
  var htmlContentMailTo = '<p>Hi,</p><p>Thank you for your e-mail. I will contact you the soonest as it is possible!</p><p>Kind regards,<br>Jagoda Kondratiuk</p><p><span style="font-style: italic;">Photographer</span><br>+48 607 901 110<br><a href="mailto:hello@jagodakondratiuk.com" target="">hello@jagodakondratiuk.com</a></p> <br><p style="font-style:italic;" >This is an automated message - please do not reply directly to this email.</p>';

  var mailTo = {
    to: req.body.email,
    subject: 'Thank you for your message',
    from: 'hello@jagodakondratiuk.com',
    sender: 'hello@jagodakondratiuk.com',
    html: htmlContentMailTo
  };
  var mailHome = {
    to: 'hello@jagodakondratiuk.com',
    subject: 'Contact from ' + req.body.email ,
    from: req.body.email,
    sender: 'hello@jagodakondratiuk.com',
    html: htmlContentMailHome
  };

  var sendResponse = function(status,send){
    res.status(status);
    res.send(send);
  }

  transporter.sendMail(mailHome, function(err, info){
    if (err) {
      sendResponse(400,'There was a problem with the server, please try again.');
      console.log('transporter error -> ', err);
    }else{
      sendResponse(200,'contact form mail send');
      console.log('Message sent Home: ' , info );
    }
  });

  transporter.sendMail(mailTo, function(err, info){
    if (err) {
      sendResponse(400,'There was a problem with the server, please try again.' );
      console.log('transporter error -> ', err);
    }else{
      console.log('Message sent To' );
    }
  });


};
